package com.extremelyurgent.triple7events;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;
import com.extremelyurgent.Services.DeleteEventService;
import com.extremelyurgent.Services.InsertChkInUsrMobService;
import com.extremelyurgent.Services.IsEventBookedService;
import com.extremelyurgent.customcontrols.Triple7EventsButton;
import com.extremelyurgent.factory.InstanceFactory;
import com.extremelyurgent.interfaces.DeleteEventTaskCompleteListener;
import com.extremelyurgent.interfaces.InsertChkInUsrMobListner;
import com.extremelyurgent.interfaces.IsEventBookedTaskCompleteListener;
import com.extremelyurgent.triple7events.db.SharedPrefs;
import com.extremelyurgent.triple7events.models.GetError;
import com.extremelyurgent.triple7events.models.InsertChkInUsrMobResponse;
import com.extremelyurgent.triple7events.models.IsEventBookedResponse;
import com.extremelyurgent.utility.CommonUtils;
import com.triple7events.android.R;

public class EventDashBoardActivity extends Fragment implements OnClickListener {

	private View rootView;
	private Activity activity;
	private Triple7EventsButton editeventBT, shareeventBT, discountcodesBT, paymentoptionBT, emailInvitationBT, deleteBT, orderBt, myaffiliateBT, payoutOptionsBT;
	private Triple7EventsButton attendeeBT;
	private Triple7EventsButton adddguestlits;
	private Triple7EventsButton checkinlistBT;
	private Triple7EventsButton dataformBT;
	private Triple7EventsButton reportBT;
	private Triple7EventsButton thankuVideoBT;
	private Triple7EventsButton reminderBT;
	private Triple7EventsButton scanBT;
	private Triple7EventsButton dataReviewFormBT;

	private int mEventID;
	private String mUserAccessLevel = "", mStartDateTime = "", mEndDateTime = "", mEventName = "", mCurrencyCode = "", mEventLink = "", mEventPic = "", mTicketEvolutionID = "";
	IsEventBookedService isEventBookedService;
	InsertChkInUsrMobService insertChkInUsrMobService;
	boolean isEventAlreadyBooked = false, canEdit = false, canDelete = false, canChangeBookingDataForm = false, isTicketEvolutionEvent = false;
	private static final int ZBAR_SCANNER_REQUEST = 0;
	SharedPrefs prefs = new SharedPrefs();

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.activity = activity;
	}

	public EventDashBoardActivity() {
		// Do Nothing

	}

	public EventDashBoardActivity(int eventID, String accessLevel, String startDateTime, String endDateTime, String eventName, String currencyCode, String eventLink, String eventPic, boolean isTicketEvolutionEvent, String mTicketEvolutionID) {
		this.mEventID = eventID;
		this.mUserAccessLevel = accessLevel;
		this.mStartDateTime = startDateTime;
		this.mEndDateTime = endDateTime;
		this.mEventName = eventName;
		this.mCurrencyCode = currencyCode;
		this.mEventLink = eventLink;
		this.mEventPic = eventPic;
		this.isTicketEvolutionEvent = isTicketEvolutionEvent;
		this.mTicketEvolutionID = mTicketEvolutionID;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		MainActivity.mToolBar.setTitle(getString(R.string.eventdashboard));
		rootView = inflater.inflate(R.layout.activity_dashboard, container, false);
		setHasOptionsMenu(true);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		insertChkInUsrMobService = InstanceFactory.getInstance(InsertChkInUsrMobService.class);
		isEventBookedService = InstanceFactory.getInstance(IsEventBookedService.class);
		String requestURL = getActivity().getString(R.string.BaseURL) + getActivity().getString(R.string.IsEventBookedMobileRequest) + mEventID;
		isEventBookedService.IsEventBooked(getActivity(), requestURL, new IsEventBookedTaskCompleteListener() {

			@Override
			public void onSuccess(List<IsEventBookedResponse> result) {
				if (result != null) {
					if (result.get(0).getData() == 1) {
						isEventAlreadyBooked = true;
					}
					else {
						isEventAlreadyBooked = false;
					}

					initView();
				}
				else {
					CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.server_error));
					CommonUtils.popFragment(getActivity());
				}

			}

			@Override
			public void onError(GetError error) {
				try {
					if (((error.getErrorMessage()) == null) || (error.getErrorMessage().equals(""))) {
						CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.server_error));
					}
					else {
						CommonUtils.showSmallToast(getActivity(), error.getErrorMessage());
					}
				}
				catch (Exception e) {
					CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.server_error));
					e.printStackTrace();
				}
				CommonUtils.popFragment(getActivity());
			}
		});
	}

	void initView() {
		editeventBT = (Triple7EventsButton) rootView.findViewById(R.id.editeventBT);
		attendeeBT = (Triple7EventsButton) rootView.findViewById(R.id.attendeeBT);
		adddguestlits = (Triple7EventsButton) rootView.findViewById(R.id.adddguestlits);
		checkinlistBT = (Triple7EventsButton) rootView.findViewById(R.id.checkinlistBT);
		dataformBT = (Triple7EventsButton) rootView.findViewById(R.id.dataformBT);
		reportBT = (Triple7EventsButton) rootView.findViewById(R.id.reportBT);
		thankuVideoBT = (Triple7EventsButton) rootView.findViewById(R.id.thankuVideoBT);
		reminderBT = (Triple7EventsButton) rootView.findViewById(R.id.reminderBT);
		scanBT = (Triple7EventsButton) rootView.findViewById(R.id.scanBT);
		shareeventBT = (Triple7EventsButton) rootView.findViewById(R.id.shareeventBT);
		discountcodesBT = (Triple7EventsButton) rootView.findViewById(R.id.discountcodesBT);
		paymentoptionBT = (Triple7EventsButton) rootView.findViewById(R.id.paymentoptionBT);
		emailInvitationBT = (Triple7EventsButton) rootView.findViewById(R.id.emailInvitationBT);
		deleteBT = (Triple7EventsButton) rootView.findViewById(R.id.deleteBT);
		orderBt = (Triple7EventsButton) rootView.findViewById(R.id.orderBT);
		myaffiliateBT = (Triple7EventsButton) rootView.findViewById(R.id.myaffiliateBT);
		dataReviewFormBT = (Triple7EventsButton) rootView.findViewById(R.id.dataRevieFormBT);
		payoutOptionsBT = (Triple7EventsButton) rootView.findViewById(R.id.payoutOptionsBT);

		thankuVideoBT.setVisibility(View.GONE);

		editeventBT.setOnClickListener(this);
		attendeeBT.setOnClickListener(this);
		adddguestlits.setOnClickListener(this);
		checkinlistBT.setOnClickListener(this);
		dataformBT.setOnClickListener(this);
		reportBT.setOnClickListener(this);
		thankuVideoBT.setOnClickListener(this);
		reminderBT.setOnClickListener(this);
		scanBT.setOnClickListener(this);
		shareeventBT.setOnClickListener(this);
		discountcodesBT.setOnClickListener(this);
		paymentoptionBT.setOnClickListener(this);
		emailInvitationBT.setOnClickListener(this);
		deleteBT.setOnClickListener(this);
		orderBt.setOnClickListener(this);
		myaffiliateBT.setOnClickListener(this);
		dataReviewFormBT.setOnClickListener(this);
		payoutOptionsBT.setOnClickListener(this);

		userAccessableLinks();
	}

	public void onClick(View view) {
		switch (view.getId()) {

		case R.id.deleteBT:
			if (canDelete) {
				deleteEventConfirmdialog(mEventName, getActivity());
			}
			else {
				CommonUtils.showlongToast(getActivity(), getString(R.string.cant_delete_event));
			}
			break;
		case R.id.editeventBT:

			if (CommonUtils.isnetworkavailable(getActivity())) {
				if (canEdit) {
					CommonUtils.replaceFragment(getActivity(), new EditEventMainActivity(mEventID), "EditEventMainActivity", true, false);
				}
				else {
					CommonUtils.showlongToast(getActivity(), getString(R.string.cant_edit_event));
				}
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;
		case R.id.shareeventBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new ShareEventsActivity(mEventLink, mEventPic, mEventName), "ShareEventsActivity", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;
		case R.id.discountcodesBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new CreateDiscountCouponActivity(mEventID, mStartDateTime, mEndDateTime), "CreateDiscountCoupon", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}

			break;
		case R.id.paymentoptionBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new PaymentOptionActivity(mEventID), "PaymentOptionActivity", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;

		case R.id.payoutOptionsBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new PaymentOptionFragment(prefs.getUserID(activity), mEventID), "PaymentOptionFragment", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;

		case R.id.emailInvitationBT:
			CommonUtils.replaceFragment(getActivity(), new EmailInvitationActivity(mEventID), "EmailInvitationActivity", true, false);
			break;
		case R.id.dataformBT:
			if (canChangeBookingDataForm) {
				CommonUtils.replaceFragment(getActivity(), new DataFormActivity(mEventID), "DataFormActivity", true, false);
			}
			else {
				CommonUtils.showlongToast(getActivity(), getString(R.string.cant_edit_booking));
			}

			break;
		case R.id.attendeeBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new AttendeeListActivity(mEventID), "AttendeeListActivity", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;
		case R.id.adddguestlits:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new AddGustListActivity(mEventID, mStartDateTime, mEndDateTime), "AddGustListActivity", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;
		case R.id.reportBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new ReportActivity(mEventID, mCurrencyCode), "ReportActivity", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;
		case R.id.checkinlistBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new CheckinListActivity(mEventID), "CheckinListActivity", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}

			break;
		case R.id.reminderBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new ReminderActivity(mEventID), "ReminderActivity", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;

		case R.id.thankuVideoBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new ThankYouVideoFragment(mEventID, mEndDateTime), "ThankYouVideoFragment", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;

		case R.id.scanBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				if (isCameraAvailable()) {
					Intent intent = new Intent(getActivity(), ZBarScannerActivity.class);
					//					intent.putExtra(ZBarConstants.SCAN_MODES, new int[] { Symbol.CODE128, Symbol.CODE39, Symbol.QRCODE });
					startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
				}
				else {
					CommonUtils.showSmallToast(getActivity(), activity.getResources().getString(R.string.camera_unavailable));
				}
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;

		case R.id.orderBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new OrderScreenActivity(mEventID, mCurrencyCode), "OrderScreenActivity", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;

		case R.id.myaffiliateBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new MyAffilatesActivity(mEventID, mCurrencyCode), "MyAffilatesActivity", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;
		case R.id.dataRevieFormBT:
			if (CommonUtils.isnetworkavailable(getActivity())) {
				CommonUtils.replaceFragment(getActivity(), new DataReviewFormActivity(mEventID), "DataReviewFormActivity", true, false);
			}
			else {
				CommonUtils.showSmallToast(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
			}
			break;

		}

	}

	/**
	 * This method will show/hide the links/view according to the access provided to the user.
	 * <p>
	 * Created on Apr 30, 2015 5:18:31 PM
	 */
	private void userAccessableLinks() {

		boolean userHasAllAccess = false;
		boolean userHasCreateAccess = false;
		boolean userHasEditAccess = false;
		boolean userHasDeleteAccess = false;

		attendeeBT.setVisibility(View.GONE);
		adddguestlits.setVisibility(View.GONE);
		checkinlistBT.setVisibility(View.GONE);
		reportBT.setVisibility(View.GONE);
		thankuVideoBT.setVisibility(View.GONE);
		reminderBT.setVisibility(View.GONE);
		scanBT.setVisibility(View.GONE);
		shareeventBT.setVisibility(View.GONE);
		discountcodesBT.setVisibility(View.GONE);
		paymentoptionBT.setVisibility(View.GONE);
		emailInvitationBT.setVisibility(View.GONE);
		myaffiliateBT.setVisibility(View.GONE);
		payoutOptionsBT.setVisibility(View.GONE);
		dataReviewFormBT.setVisibility(View.GONE);
		orderBt.setVisibility(View.GONE);

		canEdit = false;
		canDelete = false;
		canChangeBookingDataForm = false;

		if (mUserAccessLevel.contains("A")) {
			userHasAllAccess = true;
		}

		if (mUserAccessLevel.contains("C")) {
			userHasCreateAccess = true;
		}

		if (mUserAccessLevel.contains("E")) {
			userHasEditAccess = true;
		}

		if (mUserAccessLevel.contains("D")) {
			userHasDeleteAccess = true;
		}

		if (userHasAllAccess) {
			editeventBT.setVisibility(View.VISIBLE);
			attendeeBT.setVisibility(View.VISIBLE);
			adddguestlits.setVisibility(View.VISIBLE);
			checkinlistBT.setVisibility(View.VISIBLE);
			dataformBT.setVisibility(View.VISIBLE);
			reportBT.setVisibility(View.VISIBLE);
			thankuVideoBT.setVisibility(View.VISIBLE);
			reminderBT.setVisibility(View.VISIBLE);
			scanBT.setVisibility(View.VISIBLE);
			shareeventBT.setVisibility(View.VISIBLE);
			discountcodesBT.setVisibility(View.VISIBLE);
			paymentoptionBT.setVisibility(View.VISIBLE);
			emailInvitationBT.setVisibility(View.VISIBLE);
			deleteBT.setVisibility(View.VISIBLE);
			myaffiliateBT.setVisibility(View.VISIBLE);
			payoutOptionsBT.setVisibility(View.VISIBLE);
			dataReviewFormBT.setVisibility(View.VISIBLE);
			orderBt.setVisibility(View.VISIBLE);

			canEdit = true;
			canDelete = true;
			canChangeBookingDataForm = true;
		}
		else {

			if (userHasCreateAccess) {
				attendeeBT.setVisibility(View.VISIBLE);
				adddguestlits.setVisibility(View.VISIBLE);
				checkinlistBT.setVisibility(View.VISIBLE);
				reportBT.setVisibility(View.VISIBLE);
				thankuVideoBT.setVisibility(View.VISIBLE);
				reminderBT.setVisibility(View.VISIBLE);
				scanBT.setVisibility(View.VISIBLE);
				shareeventBT.setVisibility(View.VISIBLE);
				discountcodesBT.setVisibility(View.VISIBLE);
				paymentoptionBT.setVisibility(View.VISIBLE);
				emailInvitationBT.setVisibility(View.VISIBLE);
				myaffiliateBT.setVisibility(View.VISIBLE);
				payoutOptionsBT.setVisibility(View.VISIBLE);
				dataReviewFormBT.setVisibility(View.VISIBLE);
				orderBt.setVisibility(View.VISIBLE);
			}

			if (userHasEditAccess) {
				checkinlistBT.setVisibility(View.VISIBLE);
				reportBT.setVisibility(View.VISIBLE);
				thankuVideoBT.setVisibility(View.VISIBLE);
				reminderBT.setVisibility(View.VISIBLE);
				scanBT.setVisibility(View.VISIBLE);
				shareeventBT.setVisibility(View.VISIBLE);
				paymentoptionBT.setVisibility(View.VISIBLE);
				dataformBT.setVisibility(View.VISIBLE);
				editeventBT.setVisibility(View.VISIBLE);
				myaffiliateBT.setVisibility(View.VISIBLE);
				payoutOptionsBT.setVisibility(View.VISIBLE);
				dataReviewFormBT.setVisibility(View.VISIBLE);
				orderBt.setVisibility(View.VISIBLE);
				canEdit = true;
				canChangeBookingDataForm = true;
			}

			if (userHasDeleteAccess) {
				deleteBT.setVisibility(View.VISIBLE);
				thankuVideoBT.setVisibility(View.VISIBLE);
				canDelete = true;
			}
		}

		if (isEventAlreadyBooked) {
			canEdit = false;
			canDelete = false;
			canChangeBookingDataForm = false;
		}

		if (isTicketEvolutionEvent) {
			deleteBT.setVisibility(View.GONE);
			editeventBT.setVisibility(View.GONE);
			discountcodesBT.setVisibility(View.GONE);
			canEdit = false;
			canDelete = false;
		}

	}

	/**
	 * Confirmation dialog for deleting an event.
	 * <p>
	 * Created on May 4, 2015 2:51:33 PM
	 * 
	 * @param eventName
	 * @param activity
	 */
	public void deleteEventConfirmdialog(final String eventName, final Activity activity) {

		final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.CustomDialogTheme));
		builder.setCancelable(false);

		if (Build.VERSION.SDK_INT > 10) {

			// Get the layout inflater
			LayoutInflater inflater = activity.getLayoutInflater();

			// Inflate and set the layout for the dialog
			// Pass null as the parent view because its going in the dialog
			// layout

			View view = inflater.inflate(R.layout.alert_delete_event, null);

			builder.setView(view);

			TextView dialogTitle = (TextView) view.findViewById(R.id.alertTitle);
			ImageView topBorderLine = (ImageView) view.findViewById(R.id.topBorderLine);

			TextView dialogMessage = (TextView) view.findViewById(R.id.alertMessage);
			dialogMessage.setText(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.delete_sure) + " </font><b><font color=\"#961717\">" + eventName + "</font></b><font color=\"#000000\">" + " ?" + "</font>"));

			CommonUtils.setTextColor(dialogTitle, activity);
			CommonUtils.setDividerColor(topBorderLine, activity);
		}
		else {
			builder.setIcon(R.drawable.icon);
			builder.setTitle(R.string.app_name);
			builder.setMessage(Html.fromHtml(getString(R.string.delete_sure) + " <b>" + eventName + "</b>" + " ?"));
		}

		builder.setPositiveButton(Html.fromHtml("<b>" + getString(R.string.yes) + "</b>"), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				builder.create().dismiss();
				deleteEvent();
			}
		});

		builder.setNegativeButton(Html.fromHtml("<b>" + getString(R.string.no) + "</b>"), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				builder.create().dismiss();
			}
		});
		builder.create().show();
	}

	/**
	 * Method to delete event
	 * <p>
	 * Created on May 4, 2015 2:52:15 PM
	 */
	private void deleteEvent() {

		DeleteEventService deleteEventService = InstanceFactory.getInstance(DeleteEventService.class);
		String requestURL = getActivity().getString(R.string.BaseURL) + getActivity().getString(R.string.DeleteEventMobileRequest) + mEventID + "&userId=" + MainActivity.sharedPrefs.getUserID(getActivity());
		deleteEventService.deleteEvent(getActivity(), requestURL, new DeleteEventTaskCompleteListener() {

			@Override
			public void onSuccess1(List<IsEventBookedResponse> result) {
				if (result != null) {
					if (result.get(0).getData() == 1) {
						CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.event_deleted));
						CommonUtils.popFragmentTopOfStack(getActivity());
					}
					else {
						CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.server_error));
					}

				}
				else {
					CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.server_error));
				}

			}

			@Override
			public void onError1(GetError error) {
				try {
					if (((error.getErrorMessage()) == null) || (error.getErrorMessage().equals(""))) {
						CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.server_error));
					}
					else {
						CommonUtils.showSmallToast(getActivity(), error.getErrorMessage());
					}
				}
				catch (Exception e) {
					CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.server_error));
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ZBAR_SCANNER_REQUEST) {

			if (resultCode == Activity.RESULT_OK) {
				String dataString = data.getStringExtra(ZBarConstants.SCAN_RESULT);
				try {

					if (!dataString.equals("")) {
						String requestURL = getActivity().getString(R.string.BaseURL) + getActivity().getString(R.string.insertCheckinStatusMobile) + mEventID + "&CheckInCode=" + dataString;
						insertChkInUsrMobService.insertChkInUsrMobile(getActivity(), requestURL, new InsertChkInUsrMobListner() {

							@Override
							public void onSuccess(List<InsertChkInUsrMobResponse> result) {
								if (result != null && result.size() > 0 && result.get(0).getData() != null) {
									if (result.get(0).getData().equals("1")) {
										CommonUtils.showSmallToast(getActivity(), activity.getResources().getString(R.string.check_in_success));

									}
									else {
										CommonUtils.showSmallToast(getActivity(), result.get(0).getData().toString().trim());
									}
								}
								else
									CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.server_error));
							}

							@Override
							public void onError(GetError error) {

								try {
									if (((error.getErrorMessage()) == null) || (error.getErrorMessage().equals(""))) {
										CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.server_error));
									}
									else {
										CommonUtils.showSmallToast(getActivity(), error.getErrorMessage());
									}
								}
								catch (Exception e) {
									CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.server_error));
									e.printStackTrace();
								}
							}
						});
					}
				}
				catch (Exception e) {
					// Do nothing
				}
			}
			else if (resultCode == Activity.RESULT_CANCELED) {
				CommonUtils.showSmallToast(getActivity(), getResources().getString(R.string.scan_cancelled));
			}
		}
	}

	/*
	 * ZBar method to detect camera
	 */
	public boolean isCameraAvailable() {
		PackageManager pm = getActivity().getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		MainActivity.shareMenuItem.setVisible(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_share:
			if (CommonUtils.isnetworkavailable(activity)) {
				shareImageAndTextResultIntent();
			}
			else {
				CommonUtils.showSmallToast(activity, getResources().getString(R.string.no_internet_connection));
			}
			break;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDestroyOptionsMenu() {
		super.onDestroyOptionsMenu();
		MainActivity.shareMenuItem.setVisible(false);
	}

	/**
	 * Share event using installed applications in the device.
	 * <p>
	 * Created on Aug 5, 2015 2:09:57 PM
	 * 
	 * @return
	 */
	private Intent shareImageAndTextResultIntent() {
		Intent shareIntent = new Intent();
		shareIntent.setAction(Intent.ACTION_SEND);
		shareIntent.putExtra(Intent.EXTRA_TEXT, mEventLink);
		//		shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(mEventPic));
		shareIntent.setType("text/plain");
		shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		startActivity(Intent.createChooser(shareIntent, "send"));
		return shareIntent;
	}
}
